/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Test;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ConditionalAttrDefContainsAssignmentRuleTest {

    /**
     * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule#ContainsAssignmentRule(int, java.lang.String, java.lang.String, java.lang.String, int)}.
     */
    @Test
    public void testContainsAssignmentRuleIntStringStringStringInt() {
	final Rule rule = new AbstractConditionalAttrDef.ContainsAssignmentRule(15, "lhs", "=", "\\d{4}", 0);
	assertEquals("3917", rule.getValue(Collections.singletonMap(15, "lhs = 3917")));
	assertEquals("2112", rule.getValue(Collections.singletonMap(15, "lhs=2112")));
	assertEquals("5309", rule.getValue(Collections.singletonMap(15, "lhs=5309; other stuff")));
	assertEquals("1984", rule.getValue(Collections.singletonMap(15, "lhs=1984\rand more")));
	assertNull(rule.getValue(Collections.singletonMap(15, "rhs = 3917")));
	assertNull(rule.getValue(Collections.singletonMap(15, "lhs : 3917")));
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule#ContainsAssignmentRule(int, java.lang.String, java.lang.String, int)}.
     */
    @Test
    public void testContainsAssignmentRuleIntStringStringInt() {
	final Rule rule = new AbstractConditionalAttrDef.ContainsAssignmentRule(27, "lhs", "~", 0);
	assertEquals("foo", rule.getValue(Collections.singletonMap(27, "lhs ~ foo; more")));
	assertNull(rule.getValue(Collections.singletonMap(19, "lhs ~ bar^baz; yak")));
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule#ContainsAssignmentRule(int, java.lang.String, java.lang.String)}.
     */
    @Test
    public void testContainsAssignmentRuleIntStringString() {
	final Rule rule = new AbstractConditionalAttrDef.ContainsAssignmentRule(19, "x", "==");
	assertEquals("bar", rule.getValue(Collections.singletonMap(19, "x == bar;")));
	assertNull(rule.getValue(Collections.singletonMap(19, "y == baz")));
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule#ContainsAssignmentRule(int, java.lang.String, int)}.
     */
    @Test
    public void testContainsAssignmentRuleIntStringInt() {
	final Rule rule = new AbstractConditionalAttrDef.ContainsAssignmentRule(1, "y", 0);
	assertEquals("baz", rule.getValue(Collections.singletonMap(1, "y: baz\nand more")));
	assertNull(rule.getValue(Collections.singletonMap(1, "y = baz")));
   }

    /**
     * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule#ContainsAssignmentRule(int, java.lang.String)}.
     */
    @Test
    public void testContainsAssignmentRuleIntString() {
	final Rule rule = new AbstractConditionalAttrDef.ContainsAssignmentRule(2, "z");
	assertEquals("yak", rule.getValue(Collections.singletonMap(2, "z:yak")));
	assertNull(rule.getValue(Collections.singletonMap(2, "z:-bar")));
    }

}
