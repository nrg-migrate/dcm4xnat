/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.MatchesPatternRule;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ConditionalAttrDefMatchesPatternRuleTest extends TestCase {

  /**
   * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.MatchesPatternRule#getTag()}.
   */
  public void testGetTag() {
    final Rule rule = new MatchesPatternRule(42, "(my-(?:\\d{4})).*", 1);
    assertEquals(42, rule.getTag());
  }

  /**
   * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.MatchesPatternRule#getValue(java.util.Map)}.
   */
  public void testGetValue() {
    final Rule rule = new MatchesPatternRule(42, "(my-(?:\\d{4})).*", 1);
    assertEquals("my-1234", rule.getValue(Collections.singletonMap(42, "my-1234 should match")));
    assertEquals(null, rule.getValue(Collections.singletonMap(42, "my-abcd shouldn't")));
    assertEquals(null, rule.getValue(Collections.singletonMap(43, "my-1234 has the wrong tag")));
    
    final Map<Integer,String> map = new HashMap<Integer,String>();
    map.put(42, "my-3456 is a match");
    map.put(44, "my-7890 would be if it had the right tag");
    assertEquals("my-3456", rule.getValue(map));
    
    final Rule ciRule = new MatchesPatternRule(13, "Foo:\\s*(\\w+)", Pattern.CASE_INSENSITIVE, 1);
    assertEquals("bar", ciRule.getValue(Collections.singletonMap(13, "Foo:bar")));
    assertEquals("baz", ciRule.getValue(Collections.singletonMap(13, "fOo:baz")));
    assertEquals("yak", ciRule.getValue(Collections.singletonMap(13, "foO: yak")));
    assertNull(ciRule.getValue(Collections.singletonMap(13, "fool:bar")));
  }
}
