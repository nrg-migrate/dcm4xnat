/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Collections;

import org.nrg.dcm.xnat.AbstractConditionalAttrDef.EqualsRule;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ConditionalAttrDefEqualsRuleTest extends TestCase {

  /**
   * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.SimpleValueRule#getTag()}.
   */
  public void testGetTag() {
    final EqualsRule rule = new EqualsRule(42);
    assertEquals(42, rule.getTag());
  }

  /**
   * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.SimpleValueRule#getValue(java.util.Map)}.
   */
  public void testGetValue() {
    final EqualsRule rule = new EqualsRule(13);
    assertEquals("foo", rule.getValue(Collections.singletonMap(13, "foo")));
    assertEquals(null, rule.getValue(Collections.singletonMap(14, "foo")));
  }
}
