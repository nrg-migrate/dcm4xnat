/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.nrg.dcm.xnat.AbstractConditionalAttrDef.MapRule;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class ConditionalAttrDefMapRuleTest extends TestCase {

  /**
   * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.SimpleValueRule#getTag()}.
   */
  public void testGetTag() {
    final Map<String,String> map = Collections.emptyMap();
    final MapRule rule = new MapRule(42, map);
    assertEquals(42, rule.getTag());
  }

  /**
   * Test method for {@link org.nrg.dcm.xnat.AbstractConditionalAttrDef.SimpleValueRule#getValue(java.util.Map)}.
   */
  public void testGetValue() {
    final Map<String,String> map = new HashMap<String,String>();
    map.put("a", "A");
    map.put("b", "B");
    map.put("c", "C");
    final MapRule rule = new MapRule(7, map);
    assertEquals("A", rule.getValue(Collections.singletonMap(7, "a")));
    assertEquals("B", rule.getValue(Collections.singletonMap(7, "b")));
    assertEquals("d", rule.getValue(Collections.singletonMap(7, "d")));
    assertEquals(null, rule.getValue(Collections.singletonMap(8, "a")));
  }
}
