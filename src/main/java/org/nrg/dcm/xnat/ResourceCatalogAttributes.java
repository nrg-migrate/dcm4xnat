/**
 * $Id: ResourceCatalogAttributes.java,v 1.1 2007/05/04 20:03:42 karchie Exp $
 * Copyright (c) 2007 Washington University
 */
package org.nrg.dcm.xnat;

import org.nrg.attr.ReadableAttrDefSet;
import org.nrg.dcm.AttrDefSet;

/**
 * Attributes for a resourceCatalog element,
 * used in mrScanData/file to point to a DICOM file catalog.
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
final class ResourceCatalogAttributes {
  private ResourceCatalogAttributes() {}    // no instantiation
  static public ReadableAttrDefSet<Integer,String> get() { return s; }

  static final private AttrDefSet s = new AttrDefSet();

  static {
    s.add("URI");	// filled in by 
    s.add("format", "DICOM");
    s.add("content", "RAW");
  }
}
