/**
 * Copyright (c) 2008,2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import org.dcm4che2.data.Tag;
import org.nrg.attr.AttrDefSet;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.ReadableAttrDefSet;
import org.nrg.dcm.xnat.XnatAttrDef;
import org.nrg.session.BeanBuilder;
import org.nrg.xdat.bean.XnatCtscandataFocalspotBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.bean.base.BaseElement.FormatException;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
final class CTScanAttributes {
  private CTScanAttributes() {} // no instantiation

  static public ReadableAttrDefSet<Integer,String> get() { return s; }

  static final private AttrDefSet<Integer,String> s = new AttrDefSet<Integer,String>(ImageScanAttributes.get());

  static {
    s.add(new XnatAttrDef.Time("parameters/scanTime", Tag.SeriesTime));
    s.add(new VoxelResAttribute("parameters/voxelRes"));
    s.add(new OrientationAttribute("parameters/orientation"));
    s.add("parameters/rescale/intercept", Tag.RescaleIntercept);
    s.add("parameters/rescale/slope", Tag.RescaleSlope);
    s.add("parameters/kvp", Tag.KVP);
    s.add("parameters/acquisitionNumber", Tag.AcquisitionNumber);
    s.add("parameters/imageType", Tag.ImageType);
    s.add("parameters/options", Tag.ScanOptions);
    s.add("parameters/collectionDiameter", Tag.DataCollectionDiameter);
    s.add("parameters/distanceSourceToDetector", Tag.DistanceSourceToDetector);
    s.add("parameters/distanceSourceToPatient", Tag.DistanceSourceToPatient);
    s.add("parameters/gantryTilt", Tag.GantryDetectorTilt);
    s.add("parameters/tableHeight", Tag.TableHeight);
    s.add("parameters/rotationDirection", Tag.RotationDirection);
    s.add("parameters/exposureTime", Tag.ExposureTime);
    s.add("parameters/xrayTubeCurrent", Tag.XRayTubeCurrent);
    s.add("parameters/exposure", Tag.Exposure);
    s.add("parameters/filter", Tag.FilterType);
    s.add("parameters/generatorPower", Tag.GeneratorPower);
    s.add("parameters/focalSpots/focalSpot", Tag.FocalSpots);	// requires additional processing from bean builder
    s.add("parameters/convolutionKernel", Tag.ConvolutionKernel);
    s.add("parameters/collimationWidth/single", Tag.SingleCollimationWidth);
    s.add("parameters/collimationWidth/total", Tag.TotalCollimationWidth);
    s.add("parameters/tableSpeed", Tag.TableSpeed);
    s.add("parameters/tableFeedPerRotation", Tag.TableFeedPerRotation);
    s.add("parameters/pitchFactor", Tag.SpiralPitchFactor);
    s.add(new XnatAttrDef.ValueWithAttributes("parameters/estimatedDoseSaving", Tag.EstimatedDoseSaving,
        "modulation", Tag.ExposureModulationType));
    s.add("parameters/ctDIvol", Tag.CTDIvol);
    s.add("parameters/derivation", Tag.DerivationDescription);
    s.add(new ImageFOVAttribute("parameters/fov"));
  }

  private final static Collection<String> nullValues =
    Collections.unmodifiableSet(new HashSet<String>(Arrays.asList("", "null")));

  private final static String FOCAL_SPOTS = "parameters/focalSpots/focalSpot";
  private final static BeanBuilder focalSpotsBeanBuilder = new BeanBuilder() {
    private final static String FOCAL_SPOT_SEPARATOR = "\\\\";
    public Collection<BaseElement> buildBeans(final ExtAttrValue focalSpotsValue)
    throws ConversionFailureException {
      final Collection<BaseElement> beans = new LinkedList<BaseElement>();
      final String focalSpots = focalSpotsValue.getText();
      if (null == focalSpots || nullValues.contains(focalSpots)) {
        return beans;
      }
      for (final String focalSpot : focalSpots.split(FOCAL_SPOT_SEPARATOR)) {
        final XnatCtscandataFocalspotBean focalSpotBean = new XnatCtscandataFocalspotBean();
        try {
          focalSpotBean.setFocalspot(focalSpot);
        } catch (FormatException e) {
          throw new ConversionFailureException(new XnatAttrDef.Text(FOCAL_SPOTS, Tag.FocalSpots),
              focalSpotsValue, "bad content", e);
        }
        beans.add(focalSpotBean);
      }
      return beans;
    }
  };

  private final static Map<String,BeanBuilder> beanBuilders = Collections.singletonMap(FOCAL_SPOTS, focalSpotsBeanBuilder);
  
  public final static Map<String,BeanBuilder> getBeanBuilders() { return beanBuilders; }
 }
