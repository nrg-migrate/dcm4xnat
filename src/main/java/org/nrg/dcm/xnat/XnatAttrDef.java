/**
 * $Id: XnatAttrDef.java,v 1.2.4.2 2007/10/19 20:46:18 karchie Exp $
 * Copyright (c) 2006 Washington University
 */
package org.nrg.dcm.xnat;


import java.util.Map;


import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrDef;

/**
 * Specifies an external (e.g., XNAT) attribute in terms of underlying
 * DICOM attributes.  Includes several ready-to-use subclasses for simple
 * cases:
 * 
 * Text defines an element that has only a single text value,
 *      equal to the value of a single DICOM attribute
 * AttributesOnly defines an element that has only attributes (no text),
 *      each equal to the value of a single DICOM attribute
 * @author Kevin A. Archie <karchie@nrg.wustl.edu>
 * @version $Revision: 1.2.4.2 $
 */
public interface XnatAttrDef extends ExtAttrDef<Integer,String> {
  public abstract static class Abstract extends ExtAttrDef.Abstract<Integer,String> implements XnatAttrDef {
    public Abstract(final String name, final Integer...attrs) {
      super(name, attrs);
    }
  }
  
  public static class Constant extends ExtAttrDef.Constant<Integer,String> implements XnatAttrDef {
    public Constant(final String name, final String value) {
      super(name,value);
    }
  }
  
  public static class Empty extends ExtAttrDef.Empty<Integer,String> implements XnatAttrDef {
    public Empty(final String name) { super(name); }
  }
  
  public static class Text extends ExtAttrDef.Text<Integer,String> implements XnatAttrDef {
    public Text(final String name, final int tag, final String format) {
      super(name, tag, format);
    }
    
    public Text(final String name, final int tag) { super(name, tag); }
  }
  
  public static final class OptionalWrapper extends ExtAttrDef.OptionalWrapper<Integer,String>
  implements XnatAttrDef {
    public OptionalWrapper(final XnatAttrDef attr) {
      super(attr);
    }
  }
  
  /**
   * Defines a translation from one DICOM attribute to one external numeric
   * value; throws an exception if the value is not a valid number
   */
  public static class Real extends ExtAttrDef.Abstract<Integer,String> implements XnatAttrDef {
    @Override
    public String convertText(final Map<Integer,String> attrs) throws ConversionFailureException {
      assert getAttrs().size() == 1 : "Real attribute must have exactly one tag";
      final int tag = getAttrs().get(0);
      if (attrs.get(tag) == null)
        throw new ConversionFailureException(tag, "null", getName(),
            "undefined");
      final float val;
      try {
        val = Float.parseFloat(attrs.get(tag));
      } catch (NumberFormatException e) {
        throw new ConversionFailureException(tag, attrs.get(tag), getName(),
        "not valid real number");
      }
      return Float.toString(val);
    }
    public Real(String name, int tag) { super(name, tag); }   
  }


  /**
   * Defines a translation from one DICOM attribute to one external integer value.
   * Constructor throws ConversionFailureException if the value cannot be converted to an integer
   */
  public static class Int extends ExtAttrDef.Abstract<Integer,String> implements XnatAttrDef {
    @Override
    public String convertText(final Map<Integer,String> attrs) throws ConversionFailureException {
      assert getAttrs().size() == 1 : "Integer attribute must have exactly one tag";
      final int tag = getAttrs().get(0);
      if (attrs.get(tag) == null)
        throw new ConversionFailureException(tag, "null", getName(), "undefined");
      final int val;
      try {
        val = Integer.parseInt(attrs.get(tag));
      } catch (NumberFormatException e) {
        throw new ConversionFailureException(tag, attrs.get(tag), getName(),
          "not valid integer");
      }
      return Integer.toString(val);
    }
    public Int(String name, int tag) { super(name, tag); }
  }

  
  /**
   * Defines a translation from DICOM to XML date format
   * Constructor throws ConversionFailureException if value cannot be converted
   */
  public static class Date extends ExtAttrDef.Abstract<Integer,String> implements XnatAttrDef {
    @SuppressWarnings("fallthrough")
    @Override
    public String convertText(final Map<Integer,String> attrs) throws ConversionFailureException {
      final String format = "%1$4s-%2$2s-%3$2s";
      assert getAttrs().size() == 1 : "Date attribute must have exactly one tag";
      final int tag = getAttrs().get(0);
      final String dcm = attrs.get(tag);
      if (dcm == null)
        throw new ConversionFailureException(tag, "null", getName(), "undefined");
      switch (dcm.length()) {
      case 8: // DICOM v. 3.0 and later
        return String.format(format, dcm.substring(0,4), dcm.substring(4,6), dcm.substring(6,8));

      case 10:  // old DICOM
        if (dcm.charAt(4) == '.' && dcm.charAt(7) == '.')
          return String.format(format, dcm.substring(0,4), dcm.substring(5,7), dcm.substring(8,10));
        // else fall through to throw exception

      default:
        throw new ConversionFailureException(tag, dcm, getName(), "invalid");
      }
    }
    public Date(String name, int tag) { super(name, tag); }
  }
  
  /**
   * Defines a translation from DICOM to XML time format
   * Constructor throws ConversionFailureException if value cannot be converted
   */
  public static class Time extends ExtAttrDef.Abstract<Integer,String> implements XnatAttrDef {
 //   @SuppressWarnings("fallthrough")
    @Override
    public String convertText(final Map<Integer,String> attrs) throws ConversionFailureException {
      final String format = "%1$2s:%2$2s:%3$2s";
      assert getAttrs().size() == 1 : "Time attribute must have exactly one tag";
      final int tag = getAttrs().get(0);
      final String dcm = attrs.get(tag);
      if (dcm == null)
        throw new ConversionFailureException(tag, "null", getName(), "undefined");
      if (dcm.indexOf(':') > 0) {
        // looks like old DICOM format
        if (dcm.length() >= 8 && dcm.charAt(2) == ':' && dcm.charAt(4) == ':')
          return dcm.substring(0,9);    // essentially same as XML format
      } else if (dcm.length() >= 6) {
        return String.format(format, dcm.substring(0,2), dcm.substring(2,4), dcm.substring(4,6));
      }
      throw new ConversionFailureException(tag, dcm, getName(), "invalid");
    }
    public Time(String name, int tag) { super(name, tag); }
   }
  
  /**
   * Defines an external attribute for which the value element will
   * have no text, only attributes.
   */
  public static class AttributesOnly extends ExtAttrDef.AttributesOnly<Integer,String> implements XnatAttrDef{
    public AttributesOnly(String name, String[] attrs, Integer[] tags) {
      super(name,attrs,tags);
    }

    public AttributesOnly(String name, String attr, Integer tag) {
      this(name, new String[]{attr}, new Integer[]{tag});
    }
  }
  
  public static class ValueWithAttributes extends ExtAttrDef.TextWithAttributes<Integer,String> implements XnatAttrDef {
    public ValueWithAttributes(String name, Integer valuev, final String[] attrs, final Integer[] nattrs) {
      super(name, valuev, attrs, nattrs);
    }
    
    public ValueWithAttributes(String name, Integer valuev, String attr, Integer tag) {
      this(name, valuev, new String[]{attr}, new Integer[]{tag});
    }
  }
}
