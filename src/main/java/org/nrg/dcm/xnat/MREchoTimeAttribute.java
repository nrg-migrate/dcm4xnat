/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

import org.dcm4che2.data.Tag;
import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.ExtAttrDef.Multiplex;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;

/**
 * Multiplex attribute for Echo Time (TE).  If multiple values are present, this is
 * a multiecho scan, and the values go into parameters/addParam fields named
 * MultiEcho_TE{EchoNumber} instead of parameters/te.
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class MREchoTimeAttribute extends Abstract implements XnatAttrDef,
    Multiplex<Integer, String> {
  private static final String meFormat = "MultiEcho_TE%s";
  
  /**
   * @param name
   * @param attrs
   */
  public MREchoTimeAttribute() {
    super("parameters/te", Tag.EchoTime, Tag.EchoNumbers);
  }

  /* (non-Javadoc)
   * @see org.nrg.attr.ExtAttrDef.Abstract#convertText(java.util.Map)
   */
  @Override
  public String convertText(final Map<Integer,String> attrs) throws ConversionFailureException {
    if (null == attrs.get(Tag.EchoTime))
      throw new ConversionFailureException(Tag.EchoTime, "null", getName(), "undefined");
    final float val;
    try {
      val = Float.parseFloat(attrs.get(Tag.EchoTime));
    } catch (NumberFormatException e) {
      throw new ConversionFailureException(Tag.EchoTime, attrs.get(Tag.EchoTime), getName(),
      "not valid real number");
    }
    return Float.toString(val);
  }

  
  private static final Pattern nonWordCharsPattern = Pattern.compile("[^\\w]");
  
  private static String toWordChars(final String in) {
    return null == in ? null : nonWordCharsPattern.matcher(in).replaceAll("_");
  }

  /* (non-Javadoc)
   * @see org.nrg.attr.ExtAttrDef.Multiplex#demultiplex(java.util.Map)
   */
  public ExtAttrValue demultiplex(Map<Integer,String> vals)
      throws ConversionFailureException {
     return new BasicExtAttrValue("parameters/addParam", convertText(vals),
        Collections.singletonMap("name", String.format(meFormat, toWordChars(vals.get(Tag.EchoNumbers)))));
  }

  /* (non-Javadoc)
   * @see org.nrg.attr.ExtAttrDef.Multiplex#getIndexAttribute()
   */
  public Integer getIndexAttribute() { return Tag.EchoNumbers; }
}
