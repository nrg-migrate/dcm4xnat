/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.attr.ConversionFailureException;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public class MapperConditionalAttrDef extends AbstractConditionalAttrDef {
  private final Mapper<String,String> mapper;

  /**
   * @param name
   * @param mapper
   * @param rules
   */
  public MapperConditionalAttrDef(final String name,
      final Mapper<String,String> mapper,
      final Rule... rules) {
    super(name, rules);
    this.mapper = mapper;
  }

  /*
   * (non-Javadoc)
   * @see org.nrg.attr.ExtAttrDef.Abstract#convertText(java.util.Map)
   */
  public String convertText(Map<Integer,String> vals)
  throws ConversionFailureException {
    for (final Rule rule : this) {
      final String val = rule.getValue(vals);
      if (null != val) {
        final String match = mapper.get(val);
        if (null != match) return match;
      }
    }
    return null;
  }
}
