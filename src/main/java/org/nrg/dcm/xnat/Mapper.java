/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm.xnat;

/**
 * Subset of java.util.Map functionality
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public interface Mapper<K,V> {
  /**
   * @param k key
   * @return value corresponding to key, or null if none is defined
   */
  public V get(K k);
}
