/**
 * $Id: ImageSessionAttributes.java,v 1.1.2.1 2008/03/14 17:28:51 karchie Exp $
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ReadableAttrDefSet;
import org.nrg.dcm.AttrDefSet;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
class ImageSessionAttributes {
  private ImageSessionAttributes() {}    // no instantiation
  static public ReadableAttrDefSet<Integer,String> get() { return s; }
  
  static final private AttrDefSet s = new AttrDefSet();
  static {
    // xnat:experimentData
    s.add(new XnatAttrDef.Date("date", Tag.StudyDate));
    s.add(new XnatAttrDef.Time("time", Tag.StudyTime));
    s.add("acquisition_site", Tag.InstitutionName);
   
    s.add(new ExtAttrDef.Labeled<Integer,String>(new XnatAttrDef.Text("fields/field", Tag.StudyComments),
        "name", "studyComments"));
	
    // xnat:imageSessionData
    s.add("session_type", Tag.StudyDescription);
    s.add("modality", Tag.Modality);

    s.add(new ExtAttrDef.MultiValueWrapper<Integer,String>(new XnatAttrDef.Text("scanner", Tag.StationName), ","));
    s.add("scanner/manufacturer", Tag.Manufacturer);
    s.add("scanner/model", Tag.ManufacturerModelName);

    s.add("operator", Tag.OperatorsName);
    s.add("prearchivePath");
  }
}
