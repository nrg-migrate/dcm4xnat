/**
 * $Id: MRSessionAttributes.java,v 1.6.4.4 2008/03/14 17:28:51 karchie Exp $
 * Copyright (c) 2006 Washington University
 */
package org.nrg.dcm.xnat;


import org.dcm4che2.data.Tag;

import org.nrg.attr.AttrDefSet;
import org.nrg.attr.ReadableAttrDefSet;


/**
 * mrSessionData attributes
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
final class MRSessionAttributes {
  private MRSessionAttributes() {}    // no instantiation
  static public ReadableAttrDefSet<Integer,String> get() { return s; }
  
  static final private AttrDefSet<Integer,String> s = new AttrDefSet<Integer,String>(ImageSessionAttributes.get());
  static {
    s.add("UID", Tag.StudyInstanceUID);  
    s.add("fieldStrength", Tag.MagneticFieldStrength);
    s.add("dcmAccessionNumber", Tag.AccessionNumber);
    s.add("dcmPatientId", Tag.PatientID);
    s.add("dcmPatientName", Tag.PatientName);
  }
}
