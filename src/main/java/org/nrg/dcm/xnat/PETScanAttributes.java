/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.attr.AttrDefSet;
import org.nrg.attr.ReadableAttrDefSet;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
final class PETScanAttributes {
  private PETScanAttributes() {}  // no instantation
  
  static public ReadableAttrDefSet<Integer,String> get() { return s; }

  static final private AttrDefSet<Integer,String> s = new AttrDefSet<Integer,String>(ImageScanAttributes.get());
  
  static {
    s.add(new XnatAttrDef.Time("parameters/scanTime", Tag.SeriesTime));
    s.add("parameters/facility", Tag.InstitutionName);
  }
}
