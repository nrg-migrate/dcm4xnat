/**
 * $Id: CTSessionAttributes.java,v 1.1.2.2 2008/03/18 17:19:47 karchie Exp $
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.attr.ReadableAttrDefSet;
import org.nrg.attr.AttrDefSet;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
final class CTSessionAttributes {
  private CTSessionAttributes() {}    // no instantiation
  static public ReadableAttrDefSet<Integer,String> get() { return s; }
  
  static final private AttrDefSet<Integer,String> s = new AttrDefSet<Integer,String>(ImageSessionAttributes.get());
  static {
    s.add("UID", Tag.StudyInstanceUID);  
    s.add("dcmAccessionNumber", Tag.AccessionNumber);
    s.add("dcmPatientId", Tag.PatientID);
    s.add("dcmPatientName", Tag.PatientName);
  }
}
