/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.attr.ConversionFailureException;

/**
 * Implements a label-safe attribute definition: text value is modified to
 * contain only "word" characters: [-_a-zA-Z0-9]
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class LabelAttrDef extends XnatAttrDef.Abstract implements XnatAttrDef {
  private final XnatAttrDef underlying;
  
  public LabelAttrDef(final XnatAttrDef underlying) {
    super(underlying.getName(), underlying.getAttrs().toArray(new Integer[0]));
    this.underlying = underlying;
  }

  /* (non-Javadoc)
   * @see org.nrg.attr.ExtAttrDef#convertText(java.util.Map)
   */
  public String convertText(final Map<Integer, String> vals)
      throws ConversionFailureException {
    final String value = underlying.convertText(vals);
    return null == value ? null : value.replaceAll("[^-\\w]", "_");
  }
}
