/**
 * Copyright (c) 2008 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
public final class EnumIdentityMapper<T> implements Mapper<T,T> {
  final Set<T> values;
  
  public EnumIdentityMapper(final Collection<T> values) {
    this.values = new HashSet<T>(values);
  }
  
  public EnumIdentityMapper(final T...ts) {
    this.values = new HashSet<T>(Arrays.asList(ts));
  }
  
  /*
   * (non-Javadoc)
   * @see org.nrg.dcm.xnat.ValueMatcher#matches(java.lang.Object)
   */
  public T get(final T t) {
    return values.contains(t) ? t : null;
  }
}
