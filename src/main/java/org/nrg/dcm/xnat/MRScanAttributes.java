/*
 * Copyright (c) 2006-2009 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;

import org.nrg.attr.AttrDefSet;
import org.nrg.attr.ReadableAttrDefSet;

/**
 * mrScanData attributes
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 */
@SuppressWarnings("unchecked")
class MRScanAttributes {
  private MRScanAttributes() {} // no instantiation

  static public ReadableAttrDefSet<Integer,String> get() { return s; }

  static final private AttrDefSet<Integer,String> s = new AttrDefSet(ImageScanAttributes.get());

  static {
    s.add(new XnatAttrDef.Time("parameters/scanTime", Tag.SeriesTime));
    s.add(new VoxelResAttribute("parameters/voxelRes"));
    s.add(new OrientationAttribute("parameters/orientation"));
    s.add("coil", Tag.ReceiveCoilName);
    s.add("fieldStrength", Tag.MagneticFieldStrength);
    s.add(new XnatAttrDef.Real("parameters/tr", Tag.RepetitionTime));
    s.add(new MREchoTimeAttribute());
    s.add(new XnatAttrDef.OptionalWrapper(new XnatAttrDef.Real("parameters/ti", Tag.InversionTime)));
    s.add(new XnatAttrDef.Int("parameters/flip", Tag.FlipAngle));
    s.add("parameters/sequence", Tag.SequenceName);
    s.add("parameters/imageType", Tag.ImageType);
    s.add("parameters/scanSequence", Tag.ScanningSequence);
    s.add("parameters/seqVariant", Tag.SequenceVariant);
    s.add("parameters/scanOptions", Tag.ScanOptions);
    s.add("parameters/acqType", Tag.MRAcquisitionType);
    s.add(new ImageFOVAttribute("parameters/fov"));
  }
}
