/**
 * Copyright (c) 2008-2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dcm4che2.util.TagUtils;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public abstract class AbstractConditionalAttrDef extends Abstract
implements Iterable<AbstractConditionalAttrDef.Rule> {
  public static interface Rule {
    int getTag();
    String getValue(Map<Integer,String> vals);
  }

  private static abstract class SimpleValueRule implements Rule {
    private final int tag;

    protected SimpleValueRule(final int tag) { this.tag = tag; }

    public final int getTag() { return tag; }

    protected abstract String map(String val);

    public final String getValue(final Map<Integer,String> vals) {
      return map(vals.get(tag));
    }
  }

  public static final class EqualsRule extends SimpleValueRule {
    public EqualsRule(final int tag) { super(tag); }

    protected String map(final String val) { return val; }
    
    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder(super.toString());
      sb.append(TagUtils.toString(getTag()));
      return sb.toString();
    }
  }

  public static final class MapRule extends SimpleValueRule {
    private final Map<String,String> map;

    public MapRule(final int tag, final Map<String,String> map) {
      super(tag);
      this.map = new HashMap<String,String>(map);
    }

    protected String map(final String val) { 
      final String m = map.get(val);
      return null == m ? val : m;
    }
    
    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder(super.toString());
      sb.append(TagUtils.toString(getTag()));
      sb.append(" from ").append(map);
      return sb.toString();
    }
  }


  public static final class ContainsAssignmentRule extends MatchesPatternRule implements Rule {
    private final static String START = "(?:\\A|(?:.*[\\s,;]))", OPTWS = "\\s*", END = "(?:(?:[\\s,;].*\\Z)|\\Z)",
    START_GROUP = "(", END_GROUP = ")";
    private final static String DEFAULT_VALUE_PATTERN = "\\w*";
    private final static String DEFAULT_OP = "\\:";

    public ContainsAssignmentRule(final int tag,
        final String id, final String op, final String valuePattern,
        int patternFlags) {
      super(tag, new StringBuilder(START)
      .append(id)
      .append(OPTWS).append(op).append(OPTWS)
      .append(START_GROUP).append(valuePattern).append(END_GROUP)
      .append(END)
      .toString(),
      patternFlags, 1);
    }

    public ContainsAssignmentRule(final int tag, final String id, final String op, final int patternFlags) {
      this(tag, id, op, DEFAULT_VALUE_PATTERN, patternFlags);
    }

    public ContainsAssignmentRule(final int tag, final String id, final String op) {
      this(tag, id, op, 0);
    }
    
    public ContainsAssignmentRule(final int tag, final String id, final int patternFlags) {
      this(tag, id, DEFAULT_OP, DEFAULT_VALUE_PATTERN, patternFlags);
    }
    
    public ContainsAssignmentRule(final int tag, final String id) {
      this(tag, id, 0);
    }
  }

  
  public static class MatchesPatternRule implements Rule {
    private final int tag;
    private final int group;
    private final Pattern pattern;

    public MatchesPatternRule(final int tag, final Pattern pattern, final int group) {
      this.tag = tag;
      this.pattern = pattern;
      this.group = group;
    }

    public MatchesPatternRule(final int tag, final String regex, final int group) {
      this(tag, Pattern.compile(regex), group);
    }

    public MatchesPatternRule(final int tag, final String regex, final int patternFlags, final int group) {
      this(tag, Pattern.compile(regex, patternFlags), group);
    }
    
    public final int getTag() { return tag; }

    public final String getValue(final Map<Integer,String> vals) {
      final String val = vals.get(tag);
      if (null == val) {
        return null;
      } else {
        final Matcher m = pattern.matcher(val);
        return m.matches() ? m.group(group) : null;
      }
    }
    
    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder(super.toString());
      sb.append(TagUtils.toString(tag));
      sb.append(" ~ ").append(pattern).append("[").append(group).append("]");
      return sb.toString();
    }
  }

  
  private final static Integer[] DUMMY_INT_ARRAY = {};

  private final Collection<Rule> rules;

  final private static Integer[] getTags(final Rule...rules) {
    final Collection<Integer> tags = new TreeSet<Integer>();
    for (final Rule rule : rules) {
      tags.add(rule.getTag());
    }
    return tags.toArray(DUMMY_INT_ARRAY);
  }

  AbstractConditionalAttrDef(final String name, final Rule...rules) {
    super(name, getTags(rules));
    this.rules = new ArrayList<Rule>(Arrays.asList(rules)); 
  }

  public final Iterator<Rule> iterator() {
    return rules.iterator();
  }
}
