/**
 * Copyright (c) 2008-2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.util.TagUtils;
import org.nrg.CausedIOException;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.nrg.attr.ReadableAttrDefSet;
import org.nrg.attr.Utils;
import org.nrg.attr.Utils.NotRootDir;
import org.nrg.dcm.AttrAdapter;
import org.nrg.dcm.AttrDefSet;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.dcm.EnumeratedMetadataStore;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.EqualsRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;
import org.nrg.io.RelativePathWriter;
import org.nrg.io.RelativePathWriterFactory;
import org.nrg.io.ScanCatalogFileWriterFactory;
import org.nrg.session.BeanBuilder;
import org.nrg.ulog.FileMicroLogFactory;
import org.nrg.ulog.MicroLogFactory;
import org.nrg.ulog.MicroLog;
import org.nrg.xdat.bean.CatDcmcatalogBean;
import org.nrg.xdat.bean.CatDcmentryBean;
import org.nrg.xdat.bean.XnatAddfieldBean;
import org.nrg.xdat.bean.XnatCrscandataBean;
import org.nrg.xdat.bean.XnatCrsessiondataBean;
import org.nrg.xdat.bean.XnatCtscandataBean;
import org.nrg.xdat.bean.XnatCtsessiondataBean;
import org.nrg.xdat.bean.XnatExperimentdataFieldBean;
import org.nrg.xdat.bean.XnatImagescandataBean;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.XnatOptscandataBean;
import org.nrg.xdat.bean.XnatOptsessiondataBean;
import org.nrg.xdat.bean.XnatOtherdicomscandataBean;
import org.nrg.xdat.bean.XnatPetscandataBean;
import org.nrg.xdat.bean.XnatPetsessiondataBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;
import org.nrg.xdat.bean.XnatRtimagescandataBean;
import org.nrg.xdat.bean.XnatRtsessiondataBean;
import org.nrg.xdat.bean.XnatScscandataBean;
import org.nrg.xdat.bean.XnatUsscandataBean;
import org.nrg.xdat.bean.XnatUssessiondataBean;
import org.nrg.xdat.bean.XnatXascandataBean;
import org.nrg.xdat.bean.XnatXasessiondataBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.bean.base.BaseElement.FormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generates an XNAT session metadata XML from a DICOM study.
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class SessionBuilder extends org.nrg.session.SessionBuilder {
    private final static String XML_SUFFIX = ".xml";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static SortedSet<Integer> ALL_TAGS;

    private final DicomMetadataStore store;
    private final File fsdir;
    private final String studyInstanceUID;

    private Integer nScans = null;
    private final AttrDefSet sessionAttrDefs = new AttrDefSet();
    private final AttrDefSet scanAttrDefs = new AttrDefSet();
    private final RelativePathWriterFactory catalogWriterFactory;
    private final MicroLogFactory logWriterFactory;
    private File prearcDir = null;

    // Helpers for building subbeans
    private final static Map<String,BeanBuilder> imageSessionBeanBuilders = new HashMap<String,BeanBuilder>();
    private final static Map<String,BeanBuilder> imageScanBeanBuilders = new HashMap<String,BeanBuilder>();
    static {
	imageSessionBeanBuilders.put("fields/field", new BeanBuilder() {
	    public Collection<BaseElement> buildBeans(final ExtAttrValue value) {
		final Collection<BaseElement> beans = new ArrayList<BaseElement>(1);
		final XnatExperimentdataFieldBean field = new XnatExperimentdataFieldBean();
		final String text = value.getText();
		if (null == text || "null".equals(text))
		    return beans;
		field.setField(text);
		field.setName(value.getAttrs().get("name"));
		beans.add(field);
		return beans;
	    }
	});
    }

    private final static Map<String,Class<? extends XnatImagesessiondataBean>>
    modalitySessionBeanClasses = new HashMap<String,Class<? extends XnatImagesessiondataBean>>();

    private final static Map<String,Class<? extends XnatImagesessiondataBean>> sopSessionBeanClasses =
	new HashMap<String,Class<? extends XnatImagesessiondataBean>>();

    private final static Map<String,Class<? extends XnatImagescandataBean>> scanBeanClasses =
	new HashMap<String,Class<? extends XnatImagescandataBean>>();

    private final static Map<Class<? extends XnatImagesessiondataBean>,ReadableAttrDefSet<Integer,String>>
    sessionTypeAttrs =
	new HashMap<Class<? extends XnatImagesessiondataBean>,ReadableAttrDefSet<Integer,String>>();

    private final static Map<Class<? extends XnatImagescandataBean>,ReadableAttrDefSet<Integer,String>>
    scanTypeAttrs =
	new HashMap<Class<? extends XnatImagescandataBean>,ReadableAttrDefSet<Integer,String>>();

    private final static Map<Class<? extends XnatImagesessiondataBean>,Map<String,BeanBuilder>>
    sessionBeanBuilders =
	new HashMap<Class<? extends XnatImagesessiondataBean>,Map<String,BeanBuilder>>();

    private final static Map<Class<? extends XnatImagescandataBean>,Map<String,BeanBuilder>> scanBeanBuilders =
	new HashMap<Class<? extends XnatImagescandataBean>,Map<String,BeanBuilder>>();

    private final static String[] LEAD_SOP_CLASSES = {
	UID.EnhancedMRImageStorage,
	UID.MRImageStorage,
	UID.EnhancedPETImageStorage,
	UID.PositronEmissionTomographyImageStorage,
	UID.EnhancedCTImageStorage,
	UID.CTImageStorage,
	UID.XRayAngiographicImageStorage,
	UID.UltrasoundImageStorage,
	UID.UltrasoundMultiframeImageStorage,
	UID.RTImageStorage,
	UID.RTDoseStorage,
	UID.ComputedRadiographyImageStorage,
	UID.OphthalmicTomographyImageStorage
    };

    private final static String[] LEAD_MODALITIES = {
	"MR", "PT", "CT", "XA", "US", "RT", "CR", "OPT"
    };

    private final static String UNKNOWN_MODALITY = "OT";  // DICOM: Other


    static {
	sopSessionBeanClasses.put(UID.MRImageStorage, XnatMrsessiondataBean.class);
	sopSessionBeanClasses.put(UID.EnhancedMRImageStorage, XnatMrsessiondataBean.class);
	modalitySessionBeanClasses.put("MR", XnatMrsessiondataBean.class);
	scanBeanClasses.put(UID.MRImageStorage, XnatMrscandataBean.class);
	scanBeanClasses.put(UID.EnhancedMRImageStorage, XnatMrscandataBean.class);
	sessionTypeAttrs.put(XnatMrsessiondataBean.class, MRSessionAttributes.get());
	scanTypeAttrs.put(XnatMrscandataBean.class, MRScanAttributes.get());
	sessionBeanBuilders.put(XnatMrsessiondataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));
	scanBeanBuilders.put(XnatMrscandataBean.class, new HashMap<String,BeanBuilder>(imageScanBeanBuilders));

	sopSessionBeanClasses.put(UID.PositronEmissionTomographyImageStorage, XnatPetsessiondataBean.class);
	sopSessionBeanClasses.put(UID.EnhancedPETImageStorage, XnatPetsessiondataBean.class);
	modalitySessionBeanClasses.put("PT", XnatPetsessiondataBean.class);
	scanBeanClasses.put(UID.PositronEmissionTomographyImageStorage, XnatPetscandataBean.class);
	scanBeanClasses.put(UID.EnhancedPETImageStorage, XnatPetscandataBean.class);
	sessionTypeAttrs.put(XnatPetsessiondataBean.class, PETSessionAttributes.get());
	scanTypeAttrs.put(XnatPetscandataBean.class, PETScanAttributes.get());
	sessionBeanBuilders.put(XnatPetsessiondataBean.class,
		new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));
	scanBeanBuilders.put(XnatPetscandataBean.class,
		new HashMap<String,BeanBuilder>(imageScanBeanBuilders));

	sopSessionBeanClasses.put(UID.CTImageStorage, XnatCtsessiondataBean.class);
	sopSessionBeanClasses.put(UID.EnhancedCTImageStorage, XnatCtsessiondataBean.class);
	modalitySessionBeanClasses.put("CT", XnatCtsessiondataBean.class);
	scanBeanClasses.put(UID.CTImageStorage, XnatCtscandataBean.class);
	scanBeanClasses.put(UID.EnhancedCTImageStorage, XnatCtscandataBean.class);
	sessionTypeAttrs.put(XnatCtsessiondataBean.class, CTSessionAttributes.get());
	scanTypeAttrs.put(XnatCtscandataBean.class, CTScanAttributes.get());
	sessionBeanBuilders.put(XnatCtsessiondataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));
	scanBeanBuilders.put(XnatCtscandataBean.class, new HashMap<String,BeanBuilder>(imageScanBeanBuilders));
	scanBeanBuilders.get(XnatCtscandataBean.class).putAll(CTScanAttributes.getBeanBuilders());

	sopSessionBeanClasses.put(UID.XRayAngiographicImageStorage, XnatXasessiondataBean.class);
	modalitySessionBeanClasses.put("XA", XnatXasessiondataBean.class);
	scanBeanClasses.put(UID.XRayAngiographicImageStorage, XnatXascandataBean.class);
	sessionTypeAttrs.put(XnatXasessiondataBean.class, XASessionAttributes.get());
	scanTypeAttrs.put(XnatXascandataBean.class, ImageScanAttributes.get());
	sessionBeanBuilders.put(XnatXasessiondataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));
	scanBeanBuilders.put(XnatXascandataBean.class, new HashMap<String,BeanBuilder>(imageScanBeanBuilders));

	sopSessionBeanClasses.put(UID.UltrasoundImageStorage, XnatUssessiondataBean.class);
	sopSessionBeanClasses.put(UID.UltrasoundMultiframeImageStorage, XnatUssessiondataBean.class);
	modalitySessionBeanClasses.put("US", XnatUssessiondataBean.class);
	scanBeanClasses.put(UID.UltrasoundImageStorage, XnatUsscandataBean.class);
	scanBeanClasses.put(UID.UltrasoundMultiframeImageStorage, XnatUsscandataBean.class);
	sessionTypeAttrs.put(XnatUssessiondataBean.class, USSessionAttributes.get());
	scanTypeAttrs.put(XnatUsscandataBean.class, ImageScanAttributes.get());
	sessionBeanBuilders.put(XnatUssessiondataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));
	scanBeanBuilders.put(XnatUsscandataBean.class, new HashMap<String,BeanBuilder>(imageScanBeanBuilders));

	sopSessionBeanClasses.put(UID.RTImageStorage, XnatRtsessiondataBean.class);
	sopSessionBeanClasses.put(UID.RTDoseStorage, XnatRtsessiondataBean.class);
	modalitySessionBeanClasses.put("RT", XnatRtsessiondataBean.class);
	scanBeanClasses.put(UID.RTImageStorage, XnatRtimagescandataBean.class);
	scanBeanClasses.put(UID.RTDoseStorage, XnatRtimagescandataBean.class);
	sessionTypeAttrs.put(XnatRtsessiondataBean.class, RTSessionAttributes.get());
	scanTypeAttrs.put(XnatRtimagescandataBean.class, ImageScanAttributes.get());
	sessionBeanBuilders.put(XnatRtsessiondataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));
	scanBeanBuilders.put(XnatRtimagescandataBean.class, new HashMap<String,BeanBuilder>(imageScanBeanBuilders));

	sopSessionBeanClasses.put(UID.ComputedRadiographyImageStorage, XnatCrsessiondataBean.class);
	modalitySessionBeanClasses.put("CR", XnatCrsessiondataBean.class);
	scanBeanClasses.put(UID.ComputedRadiographyImageStorage, XnatCrscandataBean.class);
	sessionTypeAttrs.put(XnatCrsessiondataBean.class, CRSessionAttributes.get());
	scanTypeAttrs.put(XnatCrscandataBean.class, ImageScanAttributes.get());
	sessionBeanBuilders.put(XnatCrsessiondataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));
	scanBeanBuilders.put(XnatCrscandataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));

	sopSessionBeanClasses.put(UID.OphthalmicTomographyImageStorage, XnatOptsessiondataBean.class);
	modalitySessionBeanClasses.put("OPT", XnatOptsessiondataBean.class);
	scanBeanClasses.put(UID.OphthalmicTomographyImageStorage, XnatOptscandataBean.class);
	sessionTypeAttrs.put(XnatOptsessiondataBean.class, OPTSessionAttributes.get());
	scanTypeAttrs.put(XnatOptscandataBean.class, ImageScanAttributes.get());
	sessionBeanBuilders.put(XnatOptsessiondataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));
	scanBeanBuilders.put(XnatOptscandataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));

	scanBeanClasses.put(UID.SecondaryCaptureImageStorage, XnatScscandataBean.class);
	scanTypeAttrs.put(XnatScscandataBean.class, ImageScanAttributes.get());
	scanBeanBuilders.put(XnatScscandataBean.class, new HashMap<String,BeanBuilder>(imageScanBeanBuilders));

	scanBeanClasses.put(UID.BasicTextSRStorage, XnatOtherdicomscandataBean.class);
	scanBeanClasses.put(UID.EnhancedSRStorage, XnatOtherdicomscandataBean.class);
	scanBeanClasses.put(UID.SiemensCSANonImageStorage, XnatOtherdicomscandataBean.class);
	scanTypeAttrs.put(XnatOtherdicomscandataBean.class, ImageScanAttributes.get());
	scanBeanBuilders.put(XnatOtherdicomscandataBean.class, new HashMap<String,BeanBuilder>(imageSessionBeanBuilders));

	final SortedSet<Integer> tags = new TreeSet<Integer>();
	tags.add(Tag.SOPClassUID);
	tags.add(Tag.SeriesNumber);
	tags.add(Tag.StudyInstanceUID);
	tags.add(Tag.SeriesInstanceUID);
	tags.add(Tag.Modality);
	tags.addAll(CatalogAttributes.get().getNativeAttrs());
	tags.addAll(ImageFileAttributes.get().getNativeAttrs());
	for (final ReadableAttrDefSet<Integer,String> attrDefs : sessionTypeAttrs.values()) {
	    tags.addAll(attrDefs.getNativeAttrs());
	}
	for (final ReadableAttrDefSet<Integer,String> attrDefs : scanTypeAttrs.values()) {
	    tags.addAll(attrDefs.getNativeAttrs());
	}
	ALL_TAGS = Collections.unmodifiableSortedSet(tags);
    }


    private static <K,C> C getInstance(final K key,
	    final Map<String,Class<? extends C>> classes) {
	final Class<? extends C> clazz = classes.get(key);
	if (null == clazz) {
	    return null;
	} else {
	    try {
		return clazz.newInstance();
	    } catch (InstantiationException e) {
		throw new RuntimeException(e);
	    } catch (IllegalAccessException e) {
		throw new RuntimeException(e);
	    }
	}
    }


    /**
     * Creates a session builder that sends output to the given Writer.
     * @param fsdir Root directory of the DICOM fileset
     * @param writer Where the XML session document gets written
     * @param sessionAttrDefs additional definitions for session-level attributes
     * @param scanAttrDefs additional definitions for scan-level attributes
     * @throws IOException
     */
    public SessionBuilder(final DicomMetadataStore store, final String studyInstanceUID,
	    final File fsdir, final Writer writer,
	    final Collection<XnatAttrDef> sessionAttrDefs,
	    final Collection<XnatAttrDef> scanAttrDefs) throws IOException {
	super(fsdir.getPath(), writer);
	this.store = store;
	this.studyInstanceUID = studyInstanceUID;

	this.fsdir = fsdir.getCanonicalFile();
	this.sessionAttrDefs.addAll(sessionAttrDefs);
	this.scanAttrDefs.addAll(scanAttrDefs);
	this.catalogWriterFactory = new ScanCatalogFileWriterFactory(fsdir);
	this.logWriterFactory = new FileMicroLogFactory(fsdir);
    }

    public SessionBuilder(final DicomMetadataStore store,
	    final File fsdir, final Writer writer,
	    final Collection<XnatAttrDef> sessionAttrDefs,
	    final Collection<XnatAttrDef> scanAttrDefs)
    throws IOException,MultipleSessionException,SQLException {
	this(store, getStudyInstanceUID(store), fsdir, writer, sessionAttrDefs, scanAttrDefs);
    }

    @SuppressWarnings("unchecked")
    public SessionBuilder(final File fsdir, final Writer writer,
	    final Collection<XnatAttrDef> sessionAttrDefs,
	    final Collection<XnatAttrDef> scanAttrDefs)
    throws IOException,MultipleSessionException,SQLException {
	this(getStore(fsdir, getTags(sessionAttrDefs, scanAttrDefs)),
		fsdir, writer,
		sessionAttrDefs, scanAttrDefs);
    }

    /**
     * Creates a session builder that sends output to the given Writer.
     * @param fsdir Root directory of the DICOM fileset
     * @param writer Where the XML session document gets written
     * @param sessionAttrs additional definitions for session-level attributes
     * @throws IOException
     */
    public SessionBuilder(final File fsdir, final Writer writer, final XnatAttrDef...sessionAttrs)
    throws IOException,MultipleSessionException,SQLException {
	this(fsdir, writer, Arrays.asList(sessionAttrs), new ArrayList<XnatAttrDef>());
    }

    /**
     * Creates a session builder that writes output to a file with the
     * same name as the file set root, plus a '.xml' suffix.
     * @param fsdir Root directory of the DICOM fileset
     * @param sessionAttrs additiona definitions for session-level attributes
     * @throws IOException
     */
    public SessionBuilder(final File fsdir, final XnatAttrDef...sessionAttrs)
    throws IOException,MultipleSessionException,SQLException {
	this(fsdir, SessionBuilder.getLockedWriter(fsdir, XML_SUFFIX), sessionAttrs);
    }

    private static String getStudyInstanceUID(final DicomMetadataStore store)
    throws IOException,MultipleSessionException,SQLException {
	try {
	    final Set<String> uids = store.getUniqueValues(Tag.StudyInstanceUID);
	    if (uids.isEmpty()) {
		throw new MultipleSessionException(new String[0]);
	    }
	    final Iterator<String> i = uids.iterator();
	    final String uid = i.next();
	    if (i.hasNext()) {
		throw new MultipleSessionException(uids.toArray(new String[0]));
	    }
	    return uid;
	} catch (ConversionFailureException e) {
	    throw new RuntimeException(e);    // UIDs should require no conversion
	}
    }

    private static DicomMetadataStore getStore(final File root, final Collection<Integer> extraTags)
    throws IOException,SQLException {
	final SortedSet<Integer> tags = new TreeSet<Integer>(ALL_TAGS);
	tags.addAll(extraTags);
	final Map<String,String> options = new LinkedHashMap<String,String>();
	if ("true".equals(System.getProperty("DicomDB.cached"))) {
	    options.put("table", "CACHED");
	}
	return new EnumeratedMetadataStore(Collections.singleton(root), tags, options, null);
    }

    private static SortedSet<Integer> getTags(final Collection<XnatAttrDef>...attrDefs) {
	final SortedSet<Integer> tags = new TreeSet<Integer>();
	for (final Collection<XnatAttrDef> defs : attrDefs) {
	    for (final XnatAttrDef def : defs) {
		tags.addAll(def.getAttrs());
	    }
	}
	return tags;
    }


    public static SortedSet<Integer> getAttributeTags() {
	return ALL_TAGS;
    }


    /**
     * Set the directory path of the containing prearchive.  If this value is non-null,
     * then file references in the session XML use paths relative to the session root
     * directory.  If null, file references are absolute.
     * @param prearc
     */
    public void setPrearchivePath(final File prearc) {
	this.prearcDir = prearc;
    }


    private static String getLead(final String[] options, final Collection<String> values) {
	for (int i = 0; i < options.length; i++) {
	    if (values.contains(options[i])) {
		return options[i];
	    }
	}
	// No valid lead value.
	return null;
    }

    private static String getLeadSOPClass(final Collection<String> classes) {
	return getLead(LEAD_SOP_CLASSES, classes);
    }

    private static String getLeadModality(final Collection<String> modalities) {
	final String lead = getLead(LEAD_MODALITIES, modalities);
	// If no lead modality found, return any modality we can find.
	return null == lead && !modalities.isEmpty() ? modalities.iterator().next() : lead;
    }


    private static XnatImagescandataBean createScanBean(final String leadSOPClass) {
	final XnatImagescandataBean scan = getInstance(leadSOPClass, scanBeanClasses);
	return null == scan ? new XnatOtherdicomscandataBean() : scan;
    }


    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    @SuppressWarnings("unchecked")
    public XnatImagesessiondataBean call()
    throws IOException,MultipleSessionException {
	logger.debug("Building session for " + studyInstanceUID);
	final MicroLog sessionLog = logWriterFactory.getLog("dcmtoxnat.log");
	try {
	    final XnatImagesessiondataBean session;
	    try {
		final String leadSOPClass = getLeadSOPClass(store.getUniqueValues(Tag.SOPClassUID));
		final XnatImagesessiondataBean sopSession = getInstance(leadSOPClass, sopSessionBeanClasses);
		if (null == sopSession) {
		    // SOP Class doesn't determine a session type.  Let's try the lead modality.
		    final String modality = getLeadModality(store.getUniqueValues(Tag.Modality));
		    session = getInstance(modality, modalitySessionBeanClasses);

		    if (null == session) {
			final String message = "Session builder not implemented for SOP class "
			    + store.getUniqueValues(Tag.SOPClassUID)
			    + " or modality " + store.getUniqueValues(Tag.Modality);
			logger.error(message);
			sessionLog.log(message);
			return null;
		    }
		} else {
		    session = sopSession;
		}
	    } catch (SQLException e) {
		throw new CausedIOException(e);
	    } catch (ConversionFailureException e) {
		throw new RuntimeException("no conversion necessary for SOP Class UID", e);
	    }

	    final AttrAdapter sessionAttrs = new AttrAdapter(store,
		    Collections.singletonMap(Tag.StudyInstanceUID, studyInstanceUID));
	    sessionAttrs.add(sessionAttrDefs);
	    sessionAttrs.add(sessionTypeAttrs.get(session.getClass()));

	    final Map<ExtAttrDef<Integer,String>,Exception> failures = new HashMap<ExtAttrDef<Integer,String>,Exception>();
	    final List<ExtAttrValue> sessionValues = getValues(sessionAttrs, failures);
	    for (final Map.Entry<ExtAttrDef<Integer,String>,Exception> me: failures.entrySet()) {
		if ("UID".equals(me.getKey().getName())) {
		    final Exception cause = me.getValue();
		    if (cause instanceof NoUniqueValueException) {
			throw new MultipleSessionException(((NoUniqueValueException)cause).getValues());
		    } else {
			throw new RuntimeException("Unable to derive UID for unexpected cause", cause);
		    }
		} else {
		    report("session", me.getKey(), me.getValue(), sessionLog);
		}
	    }

	    // prearchivePath is a special case; we set this by hand.
	    for (final ExtAttrValue val : setValues(session, sessionValues,
		    sessionBeanBuilders.get(session.getClass()), "prearchivePath")) {
		final String name = val.getName();
		if ("prearchivePath".equals(name)) {
		    if (null != prearcDir) {
			session.setPrearchivepath(StringEscapeUtils.escapeXml(prearcDir.getCanonicalPath()));
		    } 
		} else {
		    throw new RuntimeException("attribute " + val.getName() + " unexpectedly skipped");
		}
	    }

	    // Identify the scans -- one scan per value of Series Instance UID
	    final SortedMap<String,Map<String,Series>> seriesToUID =
		new TreeMap<String,Map<String,Series>>(new Utils.MaybeNumericStringComparator());
	    final Set<Map<Integer,String>> seriesIds;
	    try {
		final Map<Integer,ConversionFailureException> failed = new HashMap<Integer,ConversionFailureException>();
		final Collection<Integer> seriesIdAttrs = Arrays.asList(Tag.SeriesNumber, Tag.SeriesInstanceUID, Tag.SOPClassUID, Tag.Modality);
		seriesIds = store.getUniqueCombinations(seriesIdAttrs, failed);
		if (!failed.isEmpty()) {
		    sessionLog.log("Unable to retrieve some series-identifying attributes: " + failed);
		}
	    } catch (SQLException e) {
		throw new CausedIOException(e);
	    }

	    for (final Map<Integer,String> entry : seriesIds) {
		final String seriesInstanceUID = entry.get(Tag.SeriesInstanceUID);
		String seriesNumber = entry.get(Tag.SeriesNumber);
		if (null == seriesNumber || "".equals(seriesNumber)) {
		    seriesNumber = seriesInstanceUID.replaceAll("[^\\w]", "_");
		}
		final Map<String,Series> uidToSeries;
		if (seriesToUID.containsKey(seriesNumber)) {
		    uidToSeries = seriesToUID.get(seriesNumber);
		} else {
		    uidToSeries = new TreeMap<String,Series>();
		    seriesToUID.put(seriesNumber, uidToSeries);
		}

		if (!uidToSeries.containsKey(seriesInstanceUID)) {
		    uidToSeries.put(seriesInstanceUID, new Series(seriesNumber, seriesInstanceUID));
		}
		final Series series = uidToSeries.get(seriesInstanceUID);
		series.addModality(entry.get(Tag.Modality));
		series.addSOPClass(entry.get(Tag.SOPClassUID));
	    }

	    final Map<String,Series> scanToSeries = new LinkedHashMap<String,Series>();
	    for (final Map.Entry<String,Map<String,Series>> nuse : seriesToUID.entrySet()) {  // Number->UID->Series entry
		final String seriesNumber = nuse.getKey();
		final Map<String,Series> uids = nuse.getValue();
		assert !uids.isEmpty();
		if (uids.size() > 1) {  // each study ID looks like "{SeriesNum}-{Modality}{index}"
		    final Map<String,Integer> modalityCounts = new HashMap<String,Integer>();
		    for (final Series series : uids.values()) {
			String modality = getLeadModality(series.getModalities());
			if (null == modality) {
			    modality = UNKNOWN_MODALITY;
			}
			final int index;
			if (modalityCounts.containsKey(modality)) {
			    index = modalityCounts.get(modality) + 1;
			    modalityCounts.put(modality, index);
			} else {
			    index = 1;
			    modalityCounts.put(modality, index);
			}
			final String scanID = new StringBuilder(seriesNumber).append("-").append(modality).append(index).toString();
			scanToSeries.put(scanID, series);
		    }
		} else {
		    // Just one Series Instance UID for this Series Number; use Series Number as Scan ID
		    scanToSeries.put(seriesNumber, uids.values().iterator().next());
		}
	    }

	    nScans = scanToSeries.size();

	    for (final Map.Entry<String,Series> nse : scanToSeries.entrySet()) {  // Number->Series entry
		final String scanID = nse.getKey();
		final Series series = nse.getValue();
		final String scanSOPClass = getLeadSOPClass(series.getSOPClasses());
		final XnatImagescandataBean scan = createScanBean(scanSOPClass);
		final AttrAdapter scanAttrs = new AttrAdapter(store, scanTypeAttrs.get(scan.getClass()));

		final Map<Integer,String> scanSpec = Collections.singletonMap(Tag.SeriesInstanceUID, series.getUID());

		final Map<ExtAttrDef<Integer,String>,Exception> scanFailures = new HashMap<ExtAttrDef<Integer,String>,Exception>();
		final List<ExtAttrValue> scanValues = getValues(scanAttrs, scanSpec, scanFailures);
		for (final Map.Entry<ExtAttrDef<Integer,String>,Exception> me: scanFailures.entrySet()) {
		    report("scan " + scanID, me.getKey(), me.getValue(), sessionLog);
		}

		for (final ExtAttrValue val : setValues(scan, scanValues, scanBeanBuilders.get(scan.getClass()),
			"ID", "parameters/addParam")) {
		    if ("ID".equals(val.getName())) {
			scan.setId(scanID);
		    } else if ("parameters/addParam".equals(val.getName())) {
			final XnatAddfieldBean addField = new XnatAddfieldBean();
			addField.setName(val.getAttrs().get("name"));
			addField.setAddfield(val.getText());
			try {
			    // This is nasty.  Is there a better way?
			    final Method addFieldMethod = scan.getClass().getMethod("addParameters_addparam", XnatAddfieldBean.class);
			    addFieldMethod.invoke(scan, addField);
			} catch (Exception e) {
			    final StringBuilder sb = new StringBuilder("scan ");
			    sb.append(scanID).append(" does not include a parameters/addParam field");
			    sessionLog.log(scanID, e);
			    logger.error(sb.toString(), e);
			}
		    } else {
			logger.error("scan field " + val.getName() + " unexpectedly skipped");
		    }
		}

		final AttrAdapter fileAttrs = new AttrAdapter(store, scanSpec);
		fileAttrs.add(ImageFileAttributes.get());
		final SortedMap<File,List<ExtAttrValue>> fileValues;
		try {
		    final Map<File,List<ExtAttrValue>> unsorted = fileAttrs.getValuesForFiles();
		    final SortedMap<File,List<ExtAttrValue>> sorted =
			new TreeMap<File,List<ExtAttrValue>>(new ImageFileComparator(unsorted));
		    sorted.putAll(fileAttrs.getValuesForFiles());
		    fileValues = Collections.unmodifiableSortedMap(sorted);
		} catch (SQLException e) {
		    final String message = "Unable to evaluate file attributes for scan " + scanID;
		    logger.error(message, e);
		    sessionLog.log(message, e);
		    continue;
		}

		if (fileValues.isEmpty()) {
		    final StringBuilder sb = new StringBuilder("scan ").append(scanID).append(" contains no image files");
		    logger.error(sb.toString());
		    sessionLog.log(sb.toString());
		    continue;
		}

		// Some scan-level attributes get put into the catalog file
		final AttrAdapter catalogAttrs = new AttrAdapter(store, scanSpec);
		catalogAttrs.add(CatalogAttributes.get());
		final Map<ExtAttrDef<Integer,String>,Exception> catalogFailures = new HashMap<ExtAttrDef<Integer,String>,Exception>();
		final List<ExtAttrValue> catalogValues = getValues(catalogAttrs, scanSpec, catalogFailures);
		for (final Map.Entry<ExtAttrDef<Integer,String>,Exception> me: catalogFailures.entrySet()) {
		    report("scan " + scanID + " catalog", me.getKey(), me.getValue(), sessionLog);
		}

		final CatDcmcatalogBean catalog = new CatDcmcatalogBean();
		for (final ExtAttrValue val : setValues(catalog, catalogValues, "dimensions")) {
		    assert "dimensions".equals(val.getName());
		    // "dimensions" is set in both the series and the catalog.
		    // It's attributes-only and contains only "x" and "y";
		    // "z" must be computed from individual DICOM objects.
		    try {
			catalog.setDimensions_x(val.getAttrs().get("x"));
			catalog.setDimensions_y(val.getAttrs().get("y"));
		    } catch (final FormatException e) {
			sessionLog.log("scan " + scanID + " dimensions has invalid value", e);
		    }
		}
		catalog.setDimensions_volumes(1);	// no obvious DICOM interpretation

		final File scanDir = getCommonRoot(fileValues.keySet());
		if (null == scanDir) {
		    throw new IOException("no common root available for files in scan " + scanID);
		} else if (!isContained(scanDir, fsdir)) {
		    throw new IOException("scan " + scanID + " has common root " + scanDir
			    + " not under session directory " + fsdir);
		}

		// Write the catalog file(s) and add entries to the scan record.
		try {
		    // Add an entry for each file to the catalog, and count total number of frames (z dimension)
		    final Collection<File> secondaryFiles = new ArrayList<File>();
		    int numFrames = 0;
		    for (final Map.Entry<File,List<ExtAttrValue>> me : fileValues.entrySet()) {
			final File file = me.getKey();
			final List<ExtAttrValue> values = me.getValue();
			final ExtAttrValue sopAttr = values.remove(0);
			assert ImageFileAttributes.SOPClassUID.equals(sopAttr.getName());
			final String fileSOPClass = sopAttr.getText();
			if (null == scanSOPClass || !scanSOPClass.equals(fileSOPClass)) {
			    secondaryFiles.add(file);
			    continue;
			}
			int frames = 1;		// one frame per file unless otherwise specified
			final CatDcmentryBean entry = new CatDcmentryBean();
			for (final ExtAttrValue val : setValues(entry, me.getValue(),
				ImageFileAttributes.SOPClassUID, "URI", "numFrames")) {
			    if (ImageFileAttributes.SOPClassUID.equals(val.getName())) {
				// ignore; we're done with this attribute
			    } else if ("URI".equals(val.getName())) {
				try {
				    entry.setUri(Utils.getRelativeURI(scanDir, file));
				} catch (NotRootDir e) {
				    logger.error("session directory " + fsdir + " not root for image file " + file);
				}
			    } else {
				assert "numFrames".equals(val.getName()) : "unexpected file attribute " + val.getName();
				try {
				    frames = Integer.valueOf(val.getText());
				} catch (NumberFormatException e) {
				    logger.warn("invalid frame count value " + val.getText() + " in file " + file);
				}
			    }
			}
			catalog.addEntries_entry(entry);
			numFrames += frames;
		    }
		    scan.setFrames(numFrames);
		    if (numFrames > 0) {
			catalog.setDimensions_z(numFrames);

			final RelativePathWriter writer = catalogWriterFactory.getWriter(scanDir, scanID);
			try {
			    catalog.toXML(writer, true);

			    // Make sure the file write succeeds before adding the file element to the scan record.
			    final XnatResourcecatalogBean resourceCatalog = new XnatResourcecatalogBean();
			    // If this is in the prearc, then set a relative URI (to be completed in the transfer process).
			    // Otherwise, set the full path.
			    resourceCatalog.setUri(null == prearcDir ? writer.getFullPath() : writer.getRelativePath());
			    resourceCatalog.setLabel("DICOM");
			    resourceCatalog.setFormat("DICOM");
			    resourceCatalog.setContent("RAW");
			    scan.addFile(resourceCatalog);
			} finally {
			    writer.close();
			}
		    }

		    // We might have some secondary files (i.e., those with different SOP class)
		    // in the series as well
		    if (!secondaryFiles.isEmpty()) {
			final CatDcmcatalogBean addCatalog = new CatDcmcatalogBean();
			for (final File f : secondaryFiles) {
			    final CatDcmentryBean entry = new CatDcmentryBean();
			    for (final ExtAttrValue val : setValues(entry, fileValues.get(f), "URI", "instanceNumber", "numFrames")) {
				if ("URI".equals(val.getName())) {
				    try {
					entry.setUri(Utils.getRelativeURI(scanDir, f));
				    } catch (NotRootDir e) {
					logger.error("session directory " + fsdir + " not root for image file " + f);
				    }
				}
				// ignore instanceNumber, numFrames, as they don't really apply to secondary files
			    }
			    addCatalog.addEntries_entry(entry);
			}
			final RelativePathWriter addwriter = catalogWriterFactory.getWriter(scanDir, scanID + "_secondary");
			try {
			    addCatalog.toXML(addwriter, true);

			    final XnatResourcecatalogBean addCatalogBean = new XnatResourcecatalogBean();
			    addCatalogBean.setUri(null == prearcDir ? addwriter.getFullPath() : addwriter.getRelativePath());
			    addCatalogBean.setLabel("secondary");
			    addCatalogBean.setFormat("DICOM");
			    addCatalogBean.setContent("secondary");
			    scan.addFile(addCatalogBean);
			} finally {
			    addwriter.close();
			}
		    }
		} catch (IOException e) {
		    sessionLog.log("Unable to save catalog file for scan " + scanID, e);
		}

		session.addScans_scan(scan);
	    }
	    return session;
	} finally {
	    if (sessionLog.hasMessages()) {
		logger.info("Errors occured in processing " + fsdir.getPath() + "; see " + sessionLog);
		sessionLog.close();
	    }
	}
    }


    public final String getSessionInfo() {
	final StringBuilder sb = new StringBuilder();
	sb.append(null == nScans ? "(undetermined)" : nScans);
	sb.append(" scans");
	return sb.toString();
    }

    public final void dispose() {
	store.dispose();
    }

    private static StringBuilder startReport(final Object context, final Object subject) {
	return new StringBuilder(context.toString()).append(" attribute ").append(subject);
    }

    private static void report(final Object context, final Object subject, final Exception e, final MicroLog log) {
	try {
	    if (e instanceof ConversionFailureException) {
		report(context, subject, (ConversionFailureException)e, log);
	    } else if (e instanceof NoUniqueValueException) {
		report(context, subject, (NoUniqueValueException)e, log);
	    } else {
		final StringBuilder sb = startReport(context, subject);
		sb.append(" could not be resolved :").append(e);
		log.log(sb.toString());
	    }
	} catch (IOException e1) {
	    LoggerFactory.getLogger(SessionBuilder.class).error("Unable to write to session log " + log, e1);
	}
    }

    private static void report(final Object context, final Object subject,
	    final ConversionFailureException e, final MicroLog log) throws IOException {
	final StringBuilder sb = startReport(context, subject);
	sb.append(": DICOM attribute ").append(TagUtils.toString((Integer)e.getAttr()));
	sb.append(" has bad value (").append(e.getValue()).append("); unable to derive attribute(s) ");
	sb.append(Arrays.toString(e.getExtAttrs()));
	log.log(sb.toString());
    }

    private static void report(final Object context, final Object subject,
	    final NoUniqueValueException e, final MicroLog log) throws IOException {
	final StringBuilder sb = startReport(context, subject);
	if (0 == e.getValues().length) {
	    sb.append(" has no value");
	} else {
	    sb.append(" has multiple values: ").append(Arrays.toString(e.getValues()));
	}
	log.log(sb.toString());
    }


    private static File getCommonRoot(final Collection<File> files) {
	final Iterator<File> fi = files.iterator();
	File root = fi.next().getParentFile();
	while (fi.hasNext()) {
	    root = getCommonRoot(root, fi.next());
	}
	return root;
    }

    private static File getCommonRoot(final File f1, final File f2) {
	if (null == f1) return null;
	for (File f2p = f2.getParentFile(); null != f2p; f2p = f2p.getParentFile()) {
	    if (f1.equals(f2p)) return f1;
	}
	return getCommonRoot(f1.getParentFile(), f2);
    }

    private static boolean isContained(final File f, final File dir) {
	for (File d = f; null != d; d = d.getParentFile()) {
	    if (d.equals(dir)) return true;
	}
	return false;
    }

    private final static class ImageFileComparator implements Comparator<File> {
	private static final String SORTING_ATTR = "instanceNumber";
	private final Map<File,? extends Collection<ExtAttrValue>> values;

	ImageFileComparator(final Map<File,? extends Collection<ExtAttrValue>> values) {
	    this.values = values;
	}

	private String getAttributeValue(final File f, final String attr) {
	    if (null == values || null == values.get(f)) { return null; }
	    for (final ExtAttrValue eav : values.get(f)) {
		if (attr.equals(eav.getName())) {
		    return eav.getText();
		}
	    }
	    return null;
	}

	public int compare(final File f1, final File f2) {
	    Integer i1, i2;
	    try {
		i1 = Integer.valueOf(getAttributeValue(f1, SORTING_ATTR));
	    } catch (NumberFormatException e) {
		i1 = null;
	    }
	    try {
		i2 = Integer.valueOf(getAttributeValue(f2, SORTING_ATTR));
	    } catch (NumberFormatException e) {
		i2 = null;
	    }

	    if (null == i1) {
		return null == i2 ? f1.compareTo(f2) : -1;
	    } else if (null == i2) {
		return 1;
	    }
	    if (i1.equals(i2)) {
		return f1.compareTo(f2);    // might be distinct files; compare pathnames lexicographically
	    } else {
		return i1.compareTo(i2);
	    }
	}

    }

    private static final String PROJECT_PREFIX = "Project:";
    private static final String SUBJECT_PREFIX = "Subject:";
    private static final String SESSION_PREFIX = "Session:";

    private static final String PROJECT_SPEC_LABEL = "Project";
    private static final String SUBJECT_SPEC_LABEL = "Subject";
    private static final String SESSION_SPEC_LABEL = "Session";
    private static final String ASSIGNMENT = "\\:";
    private static final String LABEL_PATTERN = "\\w+";

    /**
     * Runs the session XML builder on the named files.
     * Uses properties to control behavior:
     * output.dir is the pathname of a directory where XML should be saved
     * @param args roots (directory pathnames) for which XML should be written
     */
    public static void main(String[] args) throws IOException,MultipleSessionException,SQLException {
	final XnatAttrDef defaultProjectAttrDef = new ConditionalAttrDef("project",
		new Rule[] {
		new ContainsAssignmentRule(Tag.PatientComments,
			PROJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
			new ContainsAssignmentRule(Tag.StudyComments,
				PROJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
				new EqualsRule(Tag.StudyDescription),
				new EqualsRule(Tag.AccessionNumber),

	});

	final XnatAttrDef defaultSubjectAttrDef = new ConditionalAttrDef("subject_ID",
		new Rule[] {
		new ContainsAssignmentRule(Tag.PatientComments,
			SUBJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
			new ContainsAssignmentRule(Tag.StudyComments,
				SUBJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
				new EqualsRule(Tag.PatientName)
	});

	final XnatAttrDef defaultSessionAttrDef = new ConditionalAttrDef("label",
		new Rule[] {
		new ContainsAssignmentRule(Tag.PatientComments,
			SESSION_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
			new ContainsAssignmentRule(Tag.StudyComments,
				SESSION_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
				new EqualsRule(Tag.PatientID)
	});

	final String outputRootName = System.getProperty("output.dir");
	final File outputRoot = (outputRootName == null) ? null : new File(outputRootName);
	if (outputRoot != null && !outputRoot.isDirectory()) {
	    System.err.println("output.dir must be a directory");
	    System.exit(-1);
	}

	if (4 < args.length) {
	    System.out.println("Usage: SessionBuilder dicom-dir [Project:project] "
		    + "[Subject:subject_id] [Session:session_id]");
	    System.exit(-1);
	}

	for (int i = 0; i < args.length; ) {
	    final File f = new File(args[i++]);
	    final File out = (outputRoot == null) ? new File(f.getPath() + ".xml") : new File(outputRoot, f.getName() + ".xml");
	    XnatAttrDef project = defaultProjectAttrDef;
	    XnatAttrDef subject = defaultSubjectAttrDef;
	    XnatAttrDef session = defaultSessionAttrDef;
	    while (i < args.length) {
		String nextArg = args[i++];
		if (nextArg.startsWith(PROJECT_PREFIX)) {
		    project = new XnatAttrDef.Constant("project", nextArg.substring(PROJECT_PREFIX.length()));
		} else if (nextArg.startsWith(SUBJECT_PREFIX)) {
		    subject = new XnatAttrDef.Constant("subject_ID", nextArg.substring(SUBJECT_PREFIX.length()));
		} else if (nextArg.startsWith(SESSION_PREFIX)) {
		    session = new XnatAttrDef.Constant("label", nextArg.substring(SESSION_PREFIX.length()));
		} else {
		    i--;  // not using nextArg here after all
		    break;
		}
	    }
	    LoggerFactory.getLogger(SessionBuilder.class).debug("starting");
	    final SessionBuilder psb = new SessionBuilder(f, new FileWriter(out),
		    new LabelAttrDef(project), new LabelAttrDef(subject), new LabelAttrDef(session));
	    if ("true".equals(System.getProperty("is.prearc", "true"))) {
		psb.setPrearchivePath(f);
	    }
	    psb.run();
	    psb.dispose();
	}
    }
}
