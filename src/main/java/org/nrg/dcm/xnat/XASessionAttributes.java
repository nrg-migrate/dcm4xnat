/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.attr.ReadableAttrDefSet;
import org.nrg.attr.AttrDefSet;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
final class XASessionAttributes {
  private XASessionAttributes() {}    // no instantiation
  static public ReadableAttrDefSet<Integer,String> get() { return s; }
  
  static final private AttrDefSet<Integer,String> s = new AttrDefSet<Integer,String>(ImageSessionAttributes.get());
  static {
    s.add("UID", Tag.StudyInstanceUID);  
    s.add("dcmAccessionNumber", Tag.AccessionNumber);
    s.add("dcmPatientId", Tag.PatientID);
    s.add("dcmPatientName", Tag.PatientName);
  }
}
