/**
 * Copyright (c) 2008,2009 Washington University
 */
package org.nrg.dcm.xnat;


import org.dcm4che2.data.Tag;
import org.nrg.attr.ReadableAttrDefSet;
import org.nrg.dcm.AttrDefSet;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
final class ImageScanAttributes {
  private ImageScanAttributes() {} // no instantiation

  static public ReadableAttrDefSet<Integer,String> get() { return s; }

  static final private AttrDefSet s = new AttrDefSet();

  static {
    s.add("ID");    // handled by session builder
    s.add("UID", Tag.SeriesInstanceUID);
    
    s.add("series_description", Tag.SeriesDescription);
    s.add("scanner", Tag.StationName);
    s.add("scanner/manufacturer", Tag.Manufacturer);
    s.add("scanner/model", Tag.ManufacturerModelName);
  }
}
