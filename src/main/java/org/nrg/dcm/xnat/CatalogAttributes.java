/**
 * Copyright (c) 2007-2009 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.attr.ReadableAttrDefSet;
import org.nrg.dcm.AttrDefSet;

/**
 * Attributes for a cat:dcmCatalog object, cataloging the image files
 * that make up a single DICOM series.
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
final class CatalogAttributes {
  private CatalogAttributes() {}    // no instantiation
  static public ReadableAttrDefSet<Integer,String> get() { return s; }

  static final private AttrDefSet s = new AttrDefSet();
  static {
    s.add("UID", Tag.SeriesInstanceUID);
    // dimensions also includes "z" and "volumes", but these are
    // handled explicitly by the SessionBuilder.
    s.add(new XnatAttrDef.AttributesOnly("dimensions",
	new String[]{"x", "y"}, new Integer[]{Tag.Columns, Tag.Rows}));
    s.add(new VoxelResAttribute("voxelRes"));
    s.add(new OrientationAttribute("orientation"));
  }
}
