/**
 * Copyright (c) 2009 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.LinkedHashMap;
import java.util.Map;

import org.dcm4che2.data.Tag;
import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.Utils;

final class ImageFOVAttribute extends XnatAttrDef.Abstract {
  ImageFOVAttribute(final String name) {
    super(name, Tag.Rows, Tag.Columns);
  }

  @Override
  public ExtAttrValue convert(final Map<Integer,String> attrs)
  throws ConversionFailureException {
    final int x, y;
    try {
      x = Integer.parseInt(attrs.get(Tag.Rows));
    } catch (NumberFormatException e) {
      throw new ConversionFailureException(Tag.Rows, attrs.get(Tag.Rows),
          getName(), "not valid integer value");
    }
    try {
      y = Integer.parseInt(attrs.get(Tag.Columns));
    } catch (NumberFormatException e) {
      throw new ConversionFailureException(Tag.Columns, attrs.get(Tag.Columns),
          getName(), "not valid integer value");
    }

    return new BasicExtAttrValue(getName(), null,
        Utils.put(new LinkedHashMap<String,String>(),
            new String[]{"x", "y"},
            new String[]{Integer.toString(x), Integer.toString(y)}));
  }

  @Override
  public String convertText(Map<Integer,String> attrs) {
    return null;
  }
}