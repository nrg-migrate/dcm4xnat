/**
 * Copyright (c) 2008,2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ConditionalAttrDef extends AbstractConditionalAttrDef {
  private final static String NULL_FORMAT = "%s";
  private final Logger logger = LoggerFactory.getLogger(ConditionalAttrDef.class);
  private final String format;
  
  public ConditionalAttrDef(final String name, final String format, final Rule...rules) {
    super(name, rules);
    this.format = format;
  }
  
  public ConditionalAttrDef(final String name, final Rule...rules) {
    this(name, NULL_FORMAT, rules);
  }
  
  /* (non-Javadoc)
   * @see org.nrg.attr.ExtAttrDef.Abstract#convertText(java.util.Map)
   */
  @Override
  public String convertText(Map<Integer, String> vals) {
    for (final Rule rule : this) {
      final String val = rule.getValue(vals);
      if (null != val && !"".equals(val)) {
        final String formatted = String.format(format, val);
        logger.trace(this + " obtained value " + formatted + " from " + rule + " on " + vals);
        return formatted;
      } else {
        logger.trace("no match for " + rule + " in " + vals);
      }
    }
    return null;
  }
}
