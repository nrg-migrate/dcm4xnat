/**
 * Copyright (c) 2006,2009 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;

import org.nrg.dcm.AttrDefSet;
import org.nrg.attr.ReadableAttrDefSet;

/**
 * Attributes for a dcmEntry (DICOM file entry for catalog)
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 * @version $Revision: 1.2.4.2 $
 */
public class ImageFileAttributes {
  private ImageFileAttributes() {}    // no instantiation
  static public ReadableAttrDefSet<Integer,String> get() { return s; }

  public static final String SOPClassUID = "SOP_Class_UID";
  
  static final private AttrDefSet s = new AttrDefSet();
  static {
    s.add(SOPClassUID, Tag.SOPClassUID);    // required by SessionBuilder
    s.add("URI");
    s.add("UID", Tag.SOPInstanceUID);
    s.add("instanceNumber", Tag.InstanceNumber);
    s.add(new XnatAttrDef.OptionalWrapper(new XnatAttrDef.Int("numFrames", Tag.NumberOfFrames)));	// used for scan dimensions
  }
}
