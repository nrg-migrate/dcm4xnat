/**
 * $Id: VoxelResAttribute.java,v 1.3.4.1 2007/11/05 14:41:30 karchie Exp $
 * Copyright (c) 2007 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.LinkedHashMap;
import java.util.Map;

import org.dcm4che2.data.Tag;

import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.Utils;

/**
 * @author Kevin A. Archie <karchie@npg.wustl.edu>
 *
 */
final class VoxelResAttribute extends XnatAttrDef.Abstract {
  VoxelResAttribute(final String name) {
    super(name, Tag.PixelSpacing, Tag.SliceThickness);
  }

  @Override
  public ExtAttrValue convert(final Map<Integer,String> attrs)
  throws ConversionFailureException {
    final String vrxy = attrs.get(Tag.PixelSpacing);
    if (null == vrxy) {
      throw new ConversionFailureException(Tag.PixelSpacing, null,
          getName(), "Pixel Spacing attribute undefined");
    }
    int delimiter = vrxy.indexOf('\\');
    if (delimiter < 0) {
      throw new ConversionFailureException(Tag.PixelSpacing, vrxy,
      "delimiter \\ is missing");
    }

    final float x, y, z;
    try {
      x = Float.parseFloat(vrxy.substring(0, delimiter));
      y = Float.parseFloat(vrxy.substring(delimiter+1));
    } catch (NumberFormatException e) {
      throw new ConversionFailureException(Tag.PixelSpacing, vrxy,
          getName(), "cannot be translated to real numbers");
    }
    final String sliceThickness = attrs.get(Tag.SliceThickness);
    if (null == sliceThickness) {
      throw new ConversionFailureException(Tag.SliceThickness, null,
          getName(), "Slice Thickness attribute undefined");
    }
    try {
      z = Float.parseFloat(attrs.get(Tag.SliceThickness));
    } catch (NumberFormatException e) {
      throw new ConversionFailureException(Tag.SliceThickness,
          attrs.get(Tag.SliceThickness), getName(),
      "cannot be translated to a real number");
    }
    return new BasicExtAttrValue(getName(), null,
        Utils.put(new LinkedHashMap<String,String>(),
            new String[]{"x", "y", "z"},
            new String[]{Float.toString(x), Float.toString(y), Float.toString(z)}));
  }

  @Override
  public String convertText(final Map<Integer,String> attrs) {
    return null;
  }
}
